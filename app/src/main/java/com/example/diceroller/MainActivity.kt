package com.example.diceroller

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    lateinit var rollButton: Button
    lateinit var resultImageView: ImageView
    lateinit var resultImageView2: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rollButton = findViewById(R.id.roll_button)
        resultImageView = findViewById(R.id.result_imageview)
        resultImageView2 = findViewById(R.id.result_imageview2)

        val dices = arrayOf(R.drawable.dice_1, R.drawable.dice_2, R.drawable.dice_3, R.drawable.dice_4,
            R.drawable.dice_5, R.drawable.dice_6)


        rollButton.setOnClickListener {
            val a = (Math.random()*6)
            val b = (Math.random()*6)

            resultImageView.visibility = View.VISIBLE
            resultImageView2.visibility = View.VISIBLE

            resultImageView.setImageResource(dices[a.toInt()])
            resultImageView2.setImageResource(dices[b.toInt()])
        }
    }
}